<?php

/**
 * @file
 * Contains views_handler_add_node_link handler.
 */

/**
 * Views add node link.
 */
class views_handler_add_node_link extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#description' => t('Use %content_type as a token for the content type name.'),
      '#default_value' => $this->options['text'],
    );
  }

  function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
     foreach ($this->view->filter as $filter) {
       if (get_class($filter) == 'views_handler_filter_node_type') {
         foreach ($filter->value as $content_type) {
           if (node_access('create', $content_type)) {
             $return .= '<li>' . l(str_replace('%content_type', node_get_types('name', $content_type), $this->options['text']), 'node/add/' . str_replace('_', '-', $content_type), array('class' => 'add-node-link')) . '</li>';
           }
         }
       }
     }
    }
    if (!empty($return)) {
      $return = '<ul class="views-add-node-links">' . $return . '</ul>';
    }
    return $return;
  }

}
