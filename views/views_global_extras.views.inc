<?php

/**
 * Implements hook_views_data()
 */
function views_global_extras_views_data() {

  $data['views']['addlink'] = array(
    'title' => t('Node add link'),
    'help' => t('Provide a node add link.'),
    'area' => array(
      'handler' => 'views_handler_add_node_link',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_handlers().
 */
function views_global_extras_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_global_extras') . '/views',
    ),
    'handlers' => array(
      // area handlers
      'views_handler_add_node_link' => array(
        'parent' => 'views_handler_area',
      ),
    ),
  );
}
